package com.example.demo;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.awt.print.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


@SpringBootApplication
@RestController
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @GetMapping("/")
    public String index() {
        return String.format("Hello World");
    }


    @GetMapping("/listPrinters")
    public List<String> getPrinters() {

        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        return Arrays.stream(printServices).map(service -> service.getName()).collect(Collectors.toList());
    }

    private PrintService getPrinceServiceByName(String name) throws NoSuchElementException {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        PrintService service = null;
        try {
            service = Arrays.stream(printServices).filter(printService -> printService.getName().equals(name)).findFirst().get();
            if (service == null) {
                throw new NoSuchElementException();
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            throw e;
        }

        return service;
    }


    @PostMapping("/print")
    public ResponseEntity<String> printPDFService(
            @RequestParam(value = "printer", defaultValue = "SAMSUNG_SE") String name
            , @RequestParam(value = "file", required = true) MultipartFile file
    ) {
        try {
            PrintService service = getPrinceServiceByName(name);
            printPDF(file, service);

        } catch (NoSuchElementException e) {
            e.printStackTrace();
            return new ResponseEntity<>("Could not find printer", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>("Could not Read Data", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Could not Read Data", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<String>(file.getOriginalFilename(), HttpStatus.OK);
    }

    private void printPDF(MultipartFile file, PrintService service) throws IOException, Exception {
        try {
            PDDocument document = PDDocument.load(file.getInputStream());
            PrinterJob job = PrinterJob.getPrinterJob();
            Pageable page = new PDFPageable(document);
            PageFormat format = new PageFormat();
            Paper paper = format.getPaper();
            paper.setSize(283.46472, 113.3856);
            paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
            format.setOrientation(PageFormat.PORTRAIT);
            format.setPaper(paper);

            Book book = new Book();
            for (int i = 0; i < page.getNumberOfPages(); i++) {
                book.append(page.getPrintable(i), format);
            }

            job.setPageable(book);
            job.setPrintService(service);
//            job.printDialog();
            job.print();
            System.out.println("printed!!");
            document.close();
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }
}


//upload file
//        try {
//            String uploadDir = "E:/upload_test";
//            Path copyLocation = Paths.get(uploadDir + File.separator + StringUtils.cleanPath(file.getOriginalFilename()));
//            Files.copy(file.getInputStream(),copyLocation, StandardCopyOption.REPLACE_EXISTING);
//        }catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseEntity<>("Could not store file " + file.getOriginalFilename() + ". Please try again!", HttpStatus.INTERNAL_SERVER_ERROR);
//        }
